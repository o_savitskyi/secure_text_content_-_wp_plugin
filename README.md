# README #

### This repository is for WordPress plugin that protects your text content ###

#### Description ####

Protect your content by disabling selection and copy function in UI (but not in source view).

Disabled:

 * CTRL+A

 * CTRL+S

 * CTRL+P

 * CTRL+C

 * CTRL+INS

 * CTRL+U

 * mouse selection

 * right click 

### How can I use it? ###

* Download this repository.
* Upload all files into **/wp-content/plugins/** on your website.
* Activate the plugin through the 'Plugins' menu in WordPress.

OR 

* Download **secure_text_content.zip** and use WordPress plugin installer.
* Activate the plugin through the 'Plugins' menu in WordPress.