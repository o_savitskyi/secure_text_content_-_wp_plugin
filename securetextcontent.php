<?php 
/* 
Plugin Name: Secure Text Content
Version: 1.0
Author: Savitskyi Oleksandr

 Description: This plugin prevents users ' action like selection and copy by mouse or keyboard.
*/

/*  Copyright 2015  Savitskyi Oleksandr (email: savitskyi.oleksandr@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
function secure_text_content() {
	wp_enqueue_script(
		'securetextcontent',
		plugins_url( '/js/securetextcontent.js' , __FILE__ ));
}
add_action( 'wp_enqueue_scripts', 'secure_text_content' );


?>


